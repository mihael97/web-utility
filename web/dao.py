from enum import Enum


class OrderDirection(Enum):
    ASCENDING = "ASC"
    DESCENDING = "DESC"
